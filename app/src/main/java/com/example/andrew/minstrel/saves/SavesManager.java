package com.example.andrew.minstrel.saves;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.example.andrew.minstrel.LightUser;
import com.example.andrew.minstrel.User;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by andrew on 22.04.18.
 */

public class SavesManager extends SQLiteOpenHelper{

    static SavesManager smStatic;

    final static String PATH = "savesDB";
    final static String DATABASE_NAME = PATH;
    final static String TABLE_SAVES = "saves";
    final static int DATABASE_VERSION = 3;

    final static String KEY_ID = "_id";
    final static String KEY_PHOTO = "photo";
    final static String KEY_FIRST_NAME = "first_name";
    final static String KEY_SECOND_NAME = "second_name";

    public SavesManager(Context act){
        super(act, DATABASE_NAME, null, DATABASE_VERSION);
        smStatic = this;
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("create table " + TABLE_SAVES + "(" + KEY_ID
                + " integer primary key, " + KEY_PHOTO + " text, "
                + KEY_FIRST_NAME + " text, " + KEY_SECOND_NAME + " text" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer){
        db.execSQL("drop table if exists " + TABLE_SAVES+";");
        onCreate(db);
    }

    public static void add(LightUser u){
        SQLiteDatabase database;

        try {
             database = smStatic.getWritableDatabase();
        }catch (NullPointerException exc){
            return;
        }
        ContentValues cv = new ContentValues();
        cv.put(KEY_ID, u.ID);

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        u.smallPhoto.compress(Bitmap.CompressFormat.PNG,100,os);
        byte[] byteArray = os.toByteArray();
        cv.put(KEY_PHOTO, Base64.encodeToString(byteArray, 0));

        cv.put(KEY_FIRST_NAME, u.first_name);
        cv.put(KEY_SECOND_NAME, u.last_name);
        System.out.println("INSERT: "+database.insert(TABLE_SAVES, null, cv));
        database.close();
    }

    public static boolean isSaved(LightUser u){
        SQLiteDatabase database;
        try {
             database = smStatic.getReadableDatabase();
        }catch (NullPointerException exc){
            return false;
        }
        Cursor cursor = database.query(TABLE_SAVES, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int iId = cursor.getColumnIndex(KEY_ID);

            do{
                if (cursor.getInt(iId) == u.ID) {
                    database.close();
                    return true;
                }
            }while (cursor.moveToNext());
        }
        database.close();
        return false;
    }

    public static ArrayList<LightUser> getAll(){
        ArrayList<LightUser> alu = new ArrayList<>();
        SQLiteDatabase database;
        try {
            database = smStatic.getReadableDatabase();
        }catch (NullPointerException exc){
            return alu;
        }
        Cursor cursor = database.query(TABLE_SAVES, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int iId = cursor.getColumnIndex(KEY_ID);
            int iPhoto = cursor.getColumnIndex(KEY_PHOTO);
            int iFName = cursor.getColumnIndex(KEY_FIRST_NAME);
            int iSName = cursor.getColumnIndex(KEY_SECOND_NAME);

            do {
                LightUser u = new LightUser();
                u.ID = cursor.getInt(iId);

                byte[] decodedString = Base64.decode(cursor.getString(iPhoto), Base64.DEFAULT);
                u.smallPhoto = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                u.first_name = cursor.getString(iFName);
                u.last_name = cursor.getString(iSName);
                alu.add(u);
            } while (cursor.moveToNext());
        }
        cursor.close();
        database.close();
        return alu;
    }

    public static void deleteItem(User u){
        SQLiteDatabase database;
        try {
            database = smStatic.getWritableDatabase();
        }catch (NullPointerException exc){
            return;
        }
        database.delete(TABLE_SAVES, "_id=?", new String[]{Integer.toString(u.ID)});
        database.close();
    }

    public void deleteAll(){
        SQLiteDatabase database = getWritableDatabase();
        database.delete(TABLE_SAVES, null, null);
        smStatic = null;
        database.close();
    }
}
