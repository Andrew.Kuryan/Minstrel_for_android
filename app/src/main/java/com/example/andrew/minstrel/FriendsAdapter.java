package com.example.andrew.minstrel;

import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by andrew on 26.06.18.
 */

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.FriendsViewHolder>{

    boolean isDownloadNeeded = false;
    ArrayList<LightUser> data = new ArrayList<>();
    View.OnClickListener listener;
    View lastSeen;

    public FriendsAdapter(ArrayList<LightUser> arr, boolean b) {
        isDownloadNeeded = b;
        if (arr != null && b) {
            data.addAll(arr);
            //data = arr;
        }
        else if (arr != null){
            //resultCopy = arr;
            data.addAll(arr);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(ArrayList<LightUser> list){
        if (list != null) {
            data = list;
        }
        notifyDataSetChanged();
    }

    public void insertItem(int pos, LightUser u){
        data.add(pos, u);
        notifyItemInserted(pos);
    }

    public void changeItem(int pos, LightUser u){
        data.set(pos, u);
        notifyItemChanged(pos);
    }

    @Override
    public FriendsAdapter.FriendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.saves_item_new, parent, false);
        return new FriendsAdapter.FriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendsAdapter.FriendsViewHolder holder, int position) {
        LightUser u = data.get(position);
        if (position == data.size()-1) {
            lastSeen = holder.itemView;
        }
        if (isDownloadNeeded) {
            NewAvaDownloader nad = new NewAvaDownloader();
            nad.execute(new Pair(holder, u));
            holder.fName.setText(u.first_name);
            holder.sName.setText(u.last_name);
            String userLink = "<a href=\"https://vk.com/".concat(u.domain).concat("\">").concat(u.domain).concat("</a>");
            holder.userID.setText(Html.fromHtml(userLink, null, null));
            holder.userID.setMovementMethod(LinkMovementMethod.getInstance());
            if (u.online == 1)
                holder.lSeen.setText("Онлайн");
            else {
                Date d = new Date(u.last_seen * 1000);
                Date cur = new Date(System.currentTimeMillis());
                if (d.getDay() == cur.getDay() && d.getMonth() == cur.getMonth() && d.getYear() == cur.getYear())
                    holder.lSeen.setText("В сети: " + MainActivity.dfOnline1.format(d));
                else
                    holder.lSeen.setText("В сети: " + MainActivity.dfOnline2.format(d));
            }
        } else {
            holder.fName.setText(u.first_name);
            holder.sName.setText(u.last_name);
            holder.userAva.setImageBitmap(u.smallPhoto);
            String userLink = "<a href=\"https://vk.com/".concat(u.domain).concat("\">").concat(u.domain).concat("</a>");
            holder.userID.setText(Html.fromHtml(userLink, null, null));
            holder.userID.setMovementMethod(LinkMovementMethod.getInstance());
            if (u.online == 1)
                holder.lSeen.setText("Онлайн");
            else {
                Date d = new Date(u.last_seen * 1000);
                Date cur = new Date(System.currentTimeMillis());
                if (d.getDay() == cur.getDay() && d.getMonth() == cur.getMonth() && d.getYear() == cur.getYear())
                    holder.lSeen.setText("В сети: " + MainActivity.dfOnline1.format(d));
                else
                    holder.lSeen.setText("В сети: " + MainActivity.dfOnline2.format(d));
            }
        }
    }

    public void setOnItemClickListener(View.OnClickListener ocl){
        listener = ocl;
    }

    public class FriendsViewHolder extends RecyclerView.ViewHolder{
        TextView userID, fName, sName, lSeen, nStories;
        ImageView userAva;
        public FriendsViewHolder(View itemView) {
            super(itemView);
            userID = itemView.findViewById(R.id.userID);
            fName = itemView.findViewById(R.id.savesFName);
            sName = itemView.findViewById(R.id.savesSName);
            lSeen = itemView.findViewById(R.id.savesLSeen);
            nStories = itemView.findViewById(R.id.savesNStories);
            userAva = itemView.findViewById(R.id.userAva);
            itemView.setTag(userID);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view);
                }
            });
        }
    }

    class NewAvaDownloader extends AsyncTask<Pair<FriendsAdapter.FriendsViewHolder, LightUser>, Integer, Pair<FriendsAdapter.FriendsViewHolder, LightUser>>{

        @Override
        protected Pair<FriendsAdapter.FriendsViewHolder, LightUser> doInBackground(Pair<FriendsAdapter.FriendsViewHolder, LightUser>...p){
            p[0].secondVal.smallPhoto = MainActivity.downloadImage(p[0].secondVal.avaSmall);
            return new Pair<>(p[0].firstVal, p[0].secondVal);
        }

        @Override
        protected void onPostExecute(Pair<FriendsAdapter.FriendsViewHolder, LightUser> p){
            p.firstVal.userAva.setImageBitmap(p.secondVal.smallPhoto);
        }
    }
}

