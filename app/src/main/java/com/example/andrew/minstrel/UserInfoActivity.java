package com.example.andrew.minstrel;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public class UserInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_info);

        //кастомная строка меню
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(false);
        ab.setCustomView(R.layout.main_info_app_bar_layout);
        ab.setDisplayShowCustomEnabled(true);
        //

        RecyclerView rv = findViewById(R.id.userInfoRV);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(new UserAdapter(MainActivity.curUserProfile.curUser));
    }

    //обработчик нажатия в кастомной строке меню
    public void closeMainInfo(View v){
        finish();
    }
}
