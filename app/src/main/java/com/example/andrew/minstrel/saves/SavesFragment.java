package com.example.andrew.minstrel.saves;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.andrew.minstrel.LightUser;
import com.example.andrew.minstrel.MainActivity;
import com.example.andrew.minstrel.R;
import com.example.andrew.minstrel.Stories;
import com.example.andrew.minstrel.User;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by andrew on 09.06.18.
 */

public class SavesFragment extends Fragment{

    SwipeRefreshLayout srl;
    RecyclerView savesList;
    SavesAdapter sla;
    static ArrayList<LightUser> usersList;

    private SavesInterface listener;

    public interface SavesInterface {
        public void onSavesItemSelected(String ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView =
                inflater.inflate(R.layout.activity_saves, container, false);

        if (getActivity() instanceof SavesInterface) {
            listener = (SavesInterface) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }

        srl = rootView.findViewById(R.id.swipe_saves);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                SavesDownloader sd = new SavesDownloader();
                sd.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, true);
            }
        });
        srl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        savesList = rootView.findViewById(R.id.savesList);
        savesList.setLayoutManager(new LinearLayoutManager(rootView.getContext()));

        SavesDownloader sd = new SavesDownloader();
        sd.execute(true);

        final EditText edit = rootView.findViewById(R.id.tfFind);
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                sla.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
        Button butDelete = rootView.findViewById(R.id.butFindDelete);
        butDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit.setText("");
            }
        });

        return rootView;
    }

    class SavesDownloader extends AsyncTask<Boolean, Integer , Boolean> {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            srl.setRefreshing(true);
        }

        @Override
        protected Boolean doInBackground(Boolean...param) {
            if (param[0])
                usersList = SavesManager.getAll();
            return param[0];
        }

        @Override
        protected void onPostExecute(Boolean b) {
            super.onPostExecute(b);
            sla = new SavesAdapter(usersList, true);
            sla.setOnItemClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String ID = ((TextView) view.getTag()).getText().toString();
                    listener.onSavesItemSelected(ID);
                }
            });
            savesList.setAdapter(sla);
            srl.setRefreshing(false);
        }
    }
}
