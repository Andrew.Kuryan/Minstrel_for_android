package com.example.andrew.minstrel;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder>{

    private Field[] fields;

    public UserAdapter(User user) {
        fields = new Field[]{
                new bdate(user.bdate),
                new sex(user.sex),
                new blacklisted(user.blacklisted),
                new activities(user.activities),
                new about(user.about)
        };
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.user_info_item, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        String name = fields[position].returnName();
        String value = fields[position].returnValue();
        holder.fieldName.setText(name);
        holder.fieldValue.setText(value);
    }

    @Override
    public int getItemCount() {
        return fields.length;
    }

    public class UserViewHolder extends RecyclerView.ViewHolder{
        TextView fieldName, fieldValue;
        public UserViewHolder(View itemView) {
            super(itemView);
            fieldName = (TextView) itemView.findViewById(R.id.userInfoFieldName);
            fieldValue = (TextView) itemView.findViewById(R.id.userInfoFieldValue);
        }
    }
}

