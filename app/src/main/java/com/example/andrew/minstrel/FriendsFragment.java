package com.example.andrew.minstrel;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.andrew.minstrel.userLists.UserLoaderable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by andrew on 26.06.18.
 */

public class FriendsFragment extends Fragment{

    static String userID;
    static UserLoaderable userLoaderable;
    TextView textLoaded, textAll;
    SwipeRefreshLayout srl;
    RecyclerView savesList;
    FriendsAdapter fla;
    LinearLayoutManager llm;
    NestedScrollView nsv;
    EditText edit;
    Button butMore;
    ArrayList<LightUser> usersList = new ArrayList<>();
    int offset = 0;
    static int size;
    Port port = new Port();
    long time1, time2;

    class Port{

        Queue<LightUser> queue = new LinkedList<>();

        synchronized void add(LightUser u){
            queue.add(u);
        }

        synchronized LightUser get(){
            return queue.remove();
        }

        boolean isEmpty(){
            return queue.isEmpty();
        }
    }

    private FriendsInterface listener;

    public interface FriendsInterface{
        public void onFriendsItemSelected(String ID);
    }

    public static FriendsFragment newInstance(String ID, UserLoaderable ul){
        userID = ID;
        userLoaderable = ul;
        return new FriendsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.activity_saves, container, false);

        if (getActivity() instanceof FriendsInterface) {
            listener = (FriendsInterface) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }

        srl = rootView.findViewById(R.id.swipe_saves);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                offset = 0;
                avaPos = 0;
                adapterPos = 0;
                usersList.clear();
                fla = null;
                System.gc();
                FriendsDownloader fd = new FriendsDownloader();
                fd.execute(userID);
            }
        });
        srl.setColorSchemeResources(android.R.color.holo_orange_light, android.R.color.holo_red_light);

        textLoaded = rootView.findViewById(R.id.textLoaded);
        textAll = rootView.findViewById(R.id.textAll);
        FloatingActionButton fabToStart = rootView.findViewById(R.id.fabToStart);
        fabToStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nsv.scrollTo(0, 0);
            }
        });
        savesList = rootView.findViewById(R.id.savesList);
        llm = new LinearLayoutManager(getContext());
        nsv = rootView.findViewById(R.id.mNestedView);
        savesList.setLayoutManager(llm);
        savesList.setItemAnimator(null);

        edit = rootView.findViewById(R.id.tfFind);

        FriendsDownloader fd = new FriendsDownloader();
        fd.execute(userID);

        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String text = charSequence.toString();
                ArrayList<LightUser> data = new ArrayList<>();
                if (text.isEmpty()){
                    savesList.setAdapter(fla);
                    textLoaded.setText(Integer.toString(fla.data.size()));
                    if (fla.data.size() == usersList.size())
                        butMore.setText("Конец списка");
                    else {
                        butMore.setText("Загрузить еще");
                        butMore.setEnabled(true);
                    }
                }
                else{
                    for (LightUser u : usersList) {
                        if (Integer.toString(u.ID).indexOf(text) == 0)
                            data.add(u);
                        else if (u.first_name.toLowerCase().indexOf(text.toLowerCase()) == 0)
                            data.add(u);
                        else if (u.last_name.toLowerCase().indexOf(text.toLowerCase()) == 0)
                            data.add(u);
                    }
                    FriendsAdapter ffa = new FriendsAdapter(data, true);
                    ffa.setOnItemClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String ID = ((TextView) view.getTag()).getText().toString();
                            listener.onFriendsItemSelected(ID);
                        }
                    });
                    savesList.setAdapter(ffa);
                    butMore.setText("Поиск");
                    butMore.setEnabled(false);
                    textLoaded.setText(Integer.toString(data.size()));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
        Button butDelete = rootView.findViewById(R.id.butFindDelete);
        butDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit.setText("");
            }
        });

        butMore = rootView.findViewById(R.id.buttonMore);
        butMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapterPos != size-1) {
                    time1 = System.currentTimeMillis();
                    srl.setRefreshing(true);
                    edit.setEnabled(false);
                    offset += Constants.deltaLists;
                    if (adapterPos == offset - 1) {
                        adapterPos++;
                        adapterHandler.post(new adapterThread());
                    }
                    if (avaPos == offset - 1) {
                        avaPos++;
                        new Thread(new avaLoaderThread()).start();
                    }
                }
            }
        });

        return rootView;
    }

    class FriendsDownloader extends AsyncTask<String, Integer, Boolean>{

        @Override
        public void onPreExecute(){
            super.onPreExecute();
            time1 = System.currentTimeMillis();
            srl.setRefreshing(true);
            edit.setEnabled(false);
        }

        @Override
        public Boolean doInBackground(String...param){
            usersList = userLoaderable.getUsersList(param[0]);//getFriends(Integer.toString(param[0]));
            size = usersList.size();
                    //getFollowers(Integer.toString(param[0]));
            return true;
        }

        @Override
        public void onPostExecute(Boolean b){
            super.onPostExecute(b);
            fla = new FriendsAdapter(new ArrayList<LightUser>(), false);
            fla.setOnItemClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String ID = ((TextView) view.getTag()).getText().toString();
                    listener.onFriendsItemSelected(ID);
                }
            });
            savesList.setAdapter(fla);
            textAll.setText(Integer.toString(size));
            adapterHandler.post(new adapterThread());
            new Thread(new avaLoaderThread()).start();
        }
    }

    Handler adapterHandler = new Handler();
    int adapterPos = 0;

    class adapterThread implements Runnable{

        @Override
        public void run() {
            LightUser u = usersList.get(adapterPos);
            port.add(u);
            fla.insertItem(adapterPos, u);
            if (adapterPos < size-1 && adapterPos < offset + Constants.deltaLists - 1){
                adapterPos++;
                adapterHandler.post(this);
            }
            else{
                textLoaded.setText(Integer.toString(adapterPos+1));
                if (adapterPos == size-1){
                    butMore.setText("Конец списка");
                }
            }
        }
    }

    Handler avaLoaderHandler = new Handler();
    int avaPos = 0;

    class avaLoaderThread implements Runnable{

        @Override
        public void run() {
                while (port.isEmpty()) {}
                final LightUser u = port.get();
                final int a = avaPos;
                u.smallPhoto = MainActivity.downloadImage(u.avaSmall);
                avaLoaderHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        fla.changeItem(a, u);
                    }
                });
            if (avaPos < size-1 && avaPos < offset + Constants.deltaLists - 1){
                avaPos++;
                new Thread(new avaLoaderThread()).start();
            }
            else {
                avaLoaderHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                        edit.setEnabled(true);
                        time2 = System.currentTimeMillis() - time1;
                        System.out.println("TIME: " + time2);
                    }
                });
            }
        }
    }
}