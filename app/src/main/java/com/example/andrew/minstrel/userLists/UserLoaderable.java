package com.example.andrew.minstrel.userLists;

import com.example.andrew.minstrel.LightUser;

import java.util.ArrayList;

/**
 * Created by andrew on 08.07.18.
 */

public interface UserLoaderable {

    public ArrayList<LightUser> getUsersList(String ID);
}
