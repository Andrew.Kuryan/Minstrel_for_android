package com.example.andrew.minstrel.saves;

import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.andrew.minstrel.LightUser;
import com.example.andrew.minstrel.MainActivity;
import com.example.andrew.minstrel.Pair;
import com.example.andrew.minstrel.R;
import com.example.andrew.minstrel.Stories;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by andrew on 30.04.18.
 */

public class SavesAdapter extends RecyclerView.Adapter<SavesAdapter.SavesViewHolder>{

    boolean isDownloadNeeded;
    ArrayList<LightUser> data = new ArrayList<>();
    ArrayList<LightUser> resultCopy = new ArrayList<>();
    View.OnClickListener listener;

    public SavesAdapter(ArrayList<LightUser> arr, boolean b) {
        isDownloadNeeded = b;
        if (arr != null && b) {
            data = arr;
        }
        else if (arr != null){
            resultCopy = arr;
        }
    }

    @Override
    public int getItemCount() {

        return data.size();
    }

    public void filter(String text) {

        if (!text.isEmpty()){
            data.clear();
            for (LightUser u : resultCopy) {
                if (Integer.toString(u.ID).indexOf(text) == 0)
                    data.add(u);
                else if (u.first_name.toLowerCase().indexOf(text.toLowerCase()) == 0)
                    data.add(u);
                else if (u.last_name.toLowerCase().indexOf(text.toLowerCase()) == 0)
                    data.add(u);
            }
            isDownloadNeeded = false;
            notifyDataSetChanged();
        }
        else{
            data.clear();
            data.addAll(resultCopy);
            isDownloadNeeded = false;
            notifyDataSetChanged();
        }
    }

    @Override
    public SavesAdapter.SavesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.saves_item, parent, false);
        return new SavesAdapter.SavesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SavesAdapter.SavesViewHolder holder, int position) {
        LightUser u = data.get(position);
        if (isDownloadNeeded) {
            holder.fName.setText(u.first_name);
            holder.sName.setText(u.last_name);
            holder.userAva.setImageBitmap(u.smallPhoto);
            ItemInfoDownloader iid = new ItemInfoDownloader();
            holder.pbItem.setVisibility(View.VISIBLE);
            iid.execute(new Pair<>(holder, u));
        } else{
            holder.fName.setText(u.first_name);
            holder.sName.setText(u.last_name);
            holder.userAva.setImageBitmap(u.smallPhoto);
            String userLink = "<a href=\"https://vk.com/" + u.domain + "\">" + u.domain + "</a>";
            holder.userID.setText(Html.fromHtml(userLink, null, null));
            holder.userID.setMovementMethod(LinkMovementMethod.getInstance());
            if (u.online == 1)
                holder.lSeen.setText("Онлайн");
            else {
                Date d = new Date(u.last_seen * 1000);
                Date cur = new Date(System.currentTimeMillis());
                if (d.getDay() == cur.getDay() && d.getMonth() == cur.getMonth() && d.getYear() == cur.getYear())
                    holder.lSeen.setText("В сети: " + MainActivity.dfOnline1.format(d));
                else
                    holder.lSeen.setText("В сети: " + MainActivity.dfOnline2.format(d));
            }
            holder.nStories.setText(Integer.toString(u.numStories));
        }
    }

    public void setOnItemClickListener(View.OnClickListener ocl){
        listener = ocl;
    }

    public class SavesViewHolder extends RecyclerView.ViewHolder{
        ProgressBar pbItem;
        TextView userID, fName, sName, lSeen, nStories;
        ImageView userAva;
        public SavesViewHolder(View itemView) {
            super(itemView);
            pbItem = itemView.findViewById(R.id.savesItemProgress);
            userID = itemView.findViewById(R.id.userID);
            fName = itemView.findViewById(R.id.savesFName);
            sName = itemView.findViewById(R.id.savesSName);
            lSeen = itemView.findViewById(R.id.savesLSeen);
            nStories = itemView.findViewById(R.id.savesNStories);
            userAva = itemView.findViewById(R.id.userAva);
            itemView.setTag(userID);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view);
                }
            });
        }
    }

    class ItemInfoDownloader extends AsyncTask<Pair<SavesViewHolder, LightUser>, Integer, Pair<SavesViewHolder, LightUser>> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected Pair<SavesViewHolder, LightUser> doInBackground(Pair<SavesViewHolder, LightUser>...p){
            LightUser u = new LightUser(Integer.toString(p[0].secondVal.ID));
            Stories stories = new Stories(p[0].secondVal.ID);
            u.numStories = stories.length;
            u.smallPhoto = p[0].secondVal.smallPhoto;
            p[0].secondVal = u;
            return p[0];
        }

        @Override
        protected void onPostExecute(Pair<SavesViewHolder, LightUser> p){
            super.onPostExecute(p);
            String userLink = "<a href=\"https://vk.com/" + p.secondVal.domain + "\">" + p.secondVal.domain + "</a>";
            p.firstVal.userID.setText(Html.fromHtml(userLink, null, null));
            p.firstVal.userID.setMovementMethod(LinkMovementMethod.getInstance());
            if (p.secondVal.online == 1)
                p.firstVal.lSeen.setText("Онлайн");
            else {
                Date d = new Date(p.secondVal.last_seen * 1000);
                Date cur = new Date(System.currentTimeMillis());
                if (d.getDay() == cur.getDay() && d.getMonth() == cur.getMonth() && d.getYear() == cur.getYear())
                    p.firstVal.lSeen.setText("В сети: " + MainActivity.dfOnline1.format(d));
                else
                    p.firstVal.lSeen.setText("В сети: " + MainActivity.dfOnline2.format(d));
            }
            p.firstVal.nStories.setText(Integer.toString(p.secondVal.numStories));
            resultCopy.add(p.secondVal);
            if (resultCopy.size() == data.size()){
                data.clear();
                data.addAll(resultCopy);
            }
            p.firstVal.pbItem.setVisibility(View.INVISIBLE);
        }
    }
}