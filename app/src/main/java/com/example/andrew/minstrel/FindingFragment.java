package com.example.andrew.minstrel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by andrew on 10.06.18.
 */

public class FindingFragment extends android.support.v4.app.Fragment {

    EditText tfFind;
    private FindingInterface listener;

    public interface FindingInterface {
        public void onIDSelected(View v);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.activity_finding, container, false);
        tfFind = rootView.findViewById(R.id.tfFind);

        if (getActivity() instanceof FindingInterface) {
            listener = (FindingInterface) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }

        return rootView;
    }

    public String getInput(){

        return tfFind.getText().toString();
    }
}