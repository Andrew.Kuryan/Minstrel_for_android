package com.example.andrew.minstrel;


abstract class Field{

    abstract String returnName();

    abstract String returnValue();

    Field(){}

}

class sex extends Field{

    int value;

    @Override
    String returnName(){return "Пол :";}

    @Override
    String returnValue() {
        return new String[]{"Не указан", "Женский", "Мужской"}[value];
    }

    public sex(int value) {
        this.value = value;
    }

}

class about extends Field{

    String value;

    @Override
    String returnName() {
        return "О себе :";
    }

    @Override
    String returnValue() {
        return value;
    }

    public about(String value) {
        this.value = value;
    }

}

class activities extends Field{

    String value;

    @Override
    String returnName() {
        return "Деятельность :";
    }

    @Override
    String returnValue() {
        return value;
    }

    public activities(String value) {
        this.value = value;
    }

}

class bdate extends Field{

    String value;

    @Override
    String returnName() {
        return "Дата рождения :";
    }

    @Override
    String returnValue() {
        return value;
    }

    public bdate(String value) {
        this.value = value;
    }

}

class blacklisted extends Field{

    int value;

    @Override
    String returnName() {
        return "Находитесь ли Вы у пользователя в черном списке :";
    }

    @Override
    String returnValue() {
        return new String[]{"Нет", "Да"}[value];
    }

    public blacklisted(int value) {
        this.value = value;
    }
}

