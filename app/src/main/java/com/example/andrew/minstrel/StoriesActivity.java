package com.example.andrew.minstrel;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.andrew.minstrel.saves.SavesAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.example.andrew.minstrel.MainActivity.downloadImage;

/**
 * Created by andrew on 28.04.18.
 */

public class StoriesActivity extends AppCompatActivity{

    Story arr[];
    int curPos = 0;

    private static final boolean AUTO_HIDE = true;
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    final static DateFormat dfStory = new SimpleDateFormat("dd.MM   HH:mm");

    ProgressBar pbVideo;
    LinearLayout ll;
    ImageView imageView;
    ImageLoader il;
    VideoView videoView;
    VideoLoader vl;
    TextView position;

    private static final int UI_ANIMATION_DELAY = 300;
    private View mControlsView;
    private View mContentView;
    private final Handler mHideHandler = new Handler();
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private boolean mVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out);
        mContentView = findViewById(R.id.linear);
        pbVideo = findViewById(R.id.videoProgress);
        ll = findViewById(R.id.linear);
        position = findViewById(R.id.position);
        mControlsView = findViewById(R.id.content_controls);
        getSupportActionBar().hide();
        ArrayList<Story> al = null;
        try {
            Bundle args = getIntent().getExtras();
            al = (ArrayList<Story>) args.getSerializable(this.getString(R.string.app_pref) + ".Stories");
        }catch (Exception exc){
            finish();
            exc.printStackTrace();
        }
        arr = new Story[al.size()];
        for (int i=0; i<al.size(); i++)
            arr[i] = al.get(i);
        curPos = 0;
        drawHistory(curPos);
    }

    public void drawHistory(int pos){
        TextView date = findViewById(R.id.date);
        switch (arr[pos].type){
            case "photo":
                ll.removeView(imageView);
                if (vl != null && !vl.isCancelled())
                    vl.cancel(true);
                if (videoView != null && videoView.isPlaying())
                    videoView.stopPlayback();
                ll.removeView(videoView);

                imageView = new ImageView(this);
                il = new ImageLoader();
                il.execute(arr[pos].url);
                break;
            case "video":
                ll.removeView(videoView);
                if (il != null && !vl.isCancelled())
                    il.cancel(true);
                ll.removeView(imageView);

                videoView = new VideoView(this);
                MediaController mc = new MediaController(this);
                mc.hide();
                videoView.setMediaController(mc);
                vl = new VideoLoader();
                vl.execute(arr[pos].url);
                break;
            default:
                return;
        }
        Date d = new Date(arr[pos].date*1000);
        date.setText(dfStory.format(d));
        position.setText(new String((curPos+1)+"/"+arr.length));
    }

    public void onButNext(View v){
        if (curPos < arr.length - 1){
            curPos++;
            drawHistory(curPos);
        }
        else{
            setResult(RESULT_OK);
            finish();
        }

    }

    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    private void hide() {
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    public void onButPrev(View v){
        if (curPos > 0){
            curPos--;
            drawHistory(curPos);
        }
    }

    public void closeHistories(View v){
        finish();
    }

    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    class VideoLoader extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            ll.addView(videoView);
            pbVideo.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String...param){
            videoView.setVideoURI(Uri.parse(param[0]));
            videoView.start();
            while (!videoView.isPlaying()){}
            return true;
        }

        @Override
        protected void onPostExecute(Boolean b){
            super.onPostExecute(b);
            pbVideo.setVisibility(View.INVISIBLE);
        }
    }

    class ImageLoader extends AsyncTask<String, Integer, Bitmap>{

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            ll.addView(imageView);
            pbVideo.setVisibility(View.VISIBLE);
        }

        @Override
        protected Bitmap doInBackground(String...param){
            Bitmap bm = downloadImage(param[0]);
            return bm;
        }

        @Override
        protected void onPostExecute(Bitmap b){
            super.onPostExecute(b);
            imageView.setImageBitmap(b);
            pbVideo.setVisibility(View.INVISIBLE);
        }
    }
}

