package com.example.andrew.minstrel;

/**
 * Created by andrew on 06.05.18.
 */

public class Pair<F, S> {

    public F firstVal;
    public S secondVal;

    public Pair(F first, S second){
        this.firstVal = first;
        this.secondVal = second;
    }
}
