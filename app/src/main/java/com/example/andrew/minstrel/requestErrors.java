package com.example.andrew.minstrel;

/**
 * Created by andrew on 15.04.18.
 */
import javax.json.JsonObject;
import java.io.PrintStream;

public class requestErrors {

    private static final PrintStream requestErrorStream = System.out;

    static boolean check(JsonObject object){
        int error_code;
        String error_msg;
        boolean check;
        if(object.containsKey("error")){
            object = object.getJsonObject("error");
            error_msg = object.getString("error_msg");
            error_code = object.getInt("error_code");
            if(error_code == 1 || error_code == 6){check = true;}
            else{requestErrorStream.println(error_msg); check = false;}
        }else{check = false;}
        return check;
    }
}