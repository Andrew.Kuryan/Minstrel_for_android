package com.example.andrew.minstrel;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.andrew.minstrel.saves.SavesFragment;
import com.example.andrew.minstrel.saves.SavesManager;
import com.example.andrew.minstrel.userLists.Follows;
import com.example.andrew.minstrel.userLists.Friends;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ProfileFragment.ProfileInterface,
        FindingFragment.FindingInterface,
        SavesFragment.SavesInterface,
        FriendsFragment.FriendsInterface{

    public final static DateFormat dfOnline1 = new SimpleDateFormat("HH:mm");
    public final static DateFormat dfOnline2 = new SimpleDateFormat("dd.MM в HH:mm");

    static int testID = 111111111;
    //static int autID = 465449158;

    final static int CHOOSE_SAVE = 1, CHOOSE_FIND = 5,
            LOOK_STORIES = 2, LOOK_INFO = 3, LOOK_FRIENDS = 4, LOOK_FOLLOWS = 6;
    SavesManager sm;

    static ProfileFragment curUserProfile;

    Fragment fragment;
    NavigationView navigationView;
    ActionBar ab;
    ImageView autUserPhoto;
    TextView autUserFirstName, autUserSecName,
            autUserNumFriends, autUserNumFollows, autUserNumStories;

    boolean isAutorized = false;
    public static User autUser = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setCustomView(R.layout.profile_app_bar);
        ab.setDisplayShowCustomEnabled(true);

        Toolbar toolbar = findViewById(R.id.profile_toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
        autUserPhoto = headerLayout.findViewById(R.id.autUserPhoto);
        autUserFirstName = headerLayout.findViewById(R.id.headFirstName);
        autUserSecName = headerLayout.findViewById(R.id.headSecName);
        autUserNumFriends = headerLayout.findViewById(R.id.headNumFriends);
        autUserNumFollows = headerLayout.findViewById(R.id.headNumFollows);
        autUserNumStories = headerLayout.findViewById(R.id.headNumStories);
        navigationView.setNavigationItemSelectedListener(this);




        sm = new SavesManager(this);
        //sm.deleteAll();


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        curUserProfile = ProfileFragment.newInstance(Integer.toString(Constants.autID));
        fragmentTransaction.add(R.id.container, curUserProfile);
        fragmentTransaction.commit();
    }



    //обработчики profile
    public void onDownloadComleted(){
        if (!isAutorized) {
            try {
                autUser = curUserProfile.getUser();
                if (autUser.bigPhoto != null) {
                    autUserPhoto.setImageBitmap(autUser.bigPhoto);
                    autUserPhoto.setVisibility(View.VISIBLE);
                }
                autUserFirstName.setText(autUser.first_name);
                autUserSecName.setText(autUser.last_name);
                autUserNumFriends.setText(Integer.toString(autUser.numFriends));
                autUserNumFollows.setText(Integer.toString(autUser.numFollows));
                autUserNumStories.setText(Integer.toString(autUser.numStories));
            } catch (NullPointerException exc) {
                System.out.println("NULL POINT IN AUT");
                exc.printStackTrace();
            }
            isAutorized = true;
        }
    }

    public void onInfoClick(View v){
        Intent intent = new Intent(this, UserInfoActivity.class);
        startActivityForResult(intent, LOOK_INFO);
    }

    public void onStoriesClick(View v){
        Story arrStories[] = curUserProfile.curUser.arrStories;
        if (arrStories != null) {
            Intent intent = new Intent(this, StoriesActivity.class);
            ArrayList<Story> al = new ArrayList<Story>();
            for (int i = 0; i< arrStories.length; i++)
                al.add(arrStories[i]);
            intent.putExtra(this.getString(R.string.app_pref) + ".Stories" , al);
            startActivityForResult(intent, LOOK_STORIES);
        }
    }
    //



    //обработчики finding
    @Override
    public void onIDSelected(View v){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        curUserProfile = ProfileFragment.newInstance(((FindingFragment) fragment).getInput());
        fragmentTransaction.replace(R.id.container, curUserProfile);
        fragmentTransaction.commit();
        resetNavMenu();
        ((TextView) ab.getCustomView().findViewById(R.id.mainTitle)).setText("Профиль");
    }
    //



    //обработчики saves
    @Override
    public void onSavesItemSelected(String ID){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        curUserProfile = ProfileFragment.newInstance(ID);
        fragmentTransaction.replace(R.id.container, curUserProfile);
        fragmentTransaction.commit();
        resetNavMenu();
        ((TextView) ab.getCustomView().findViewById(R.id.mainTitle)).setText("Профиль");
    }
    //

    //обработчики friends
    @Override
    public void onFriendsItemSelected(String ID){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        curUserProfile = ProfileFragment.newInstance(ID);
        fragmentTransaction.replace(R.id.container, curUserProfile);
        fragmentTransaction.commit();
        resetNavMenu();
        ((TextView) ab.getCustomView().findViewById(R.id.mainTitle)).setText("Профиль");
    }
    //


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        fragment = null;
        Class fragmentClass = null;

        int id = item.getItemId();
        if (id == R.id.nav_find) {
            fragmentClass = FindingFragment.class;
        } else if (id == R.id.nav_starred) {
            fragmentClass = SavesFragment.class;
        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_help) {

        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        item.setChecked(true);
        ((TextView) ab.getCustomView().findViewById(R.id.mainTitle)).setText(item.getTitle());

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onNavHeaderClick(View v){
        System.out.println("SOME CLICK");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        curUserProfile = ProfileFragment.newInstance(Integer.toString(autUser.ID));
        fragmentTransaction.replace(R.id.container, curUserProfile);
        fragmentTransaction.commit();

        resetNavMenu();
    }

    public void showFriends(View v){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragment = FriendsFragment.newInstance(Integer.toString(curUserProfile.curUser.ID), new Friends());
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }

    public void showFollows(View v){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragment = FriendsFragment.newInstance(Integer.toString(curUserProfile.curUser.ID), new Follows());
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }

    public static ProfileFragment getTestProfile(){
        ProfileFragment pf = new ProfileFragment();
        pf.curUser = test(false, System.currentTimeMillis()-9000);
        pf.setCurUser();
        return pf;
    }

    public static User test(boolean isOnline, long last_seen){
        User u = new User();
        u.ID = testID;
        u.first_name = "First_Name";
        u.last_name = "Last_Name";
        u.domain = "id111111111";
        if (isOnline){
            u.online = 1;
            u.last_seen = -1;
        }
        else {
            u.online = 0;
            u.last_seen = last_seen;
        }

        u.bdate = "1.1.1970";
        u.country = "Country";
        u.city = "City";
        u.occupType = "work";
        u.occupName = "Programming";
        u.site = "http://www.google.com";
        u.status = "Status";
        Story st[] = new Story[1];
        st[0] = new Story();
        st[0].type = "bigPhoto";
        st[0].date = System.currentTimeMillis()/1000 - 3600;
        return u;
    }

    public void resetNavMenu(){
        for (int i = 0; i < navigationView.getMenu().size(); i++)
            navigationView.getMenu().getItem(i).setChecked(false);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public static Bitmap downloadImage(String iUrl) {
        Bitmap bitmap = null;
        HttpURLConnection conn = null;
        BufferedInputStream buf_stream = null;
        try {
            conn = (HttpURLConnection) new URL(iUrl).openConnection();
            conn.setDoInput(true);
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.connect();
            buf_stream = new BufferedInputStream(conn.getInputStream());
            bitmap = BitmapFactory.decodeStream(buf_stream);
            buf_stream.close();
            conn.disconnect();
            buf_stream = null;
            conn = null;
        }catch (Exception exc){
            return null;
        } finally {
            if ( buf_stream != null )
                try { buf_stream.close(); } catch (IOException ex) {}
            if ( conn != null )
                conn.disconnect();
        }
        return bitmap;
    }

    public float dpFromPx(float px) {
        return px / getApplicationContext().getResources().getDisplayMetrics().density;
    }

    public float pxFromDp(float dp) {
        return dp * getApplicationContext().getResources().getDisplayMetrics().density;
    }
}

