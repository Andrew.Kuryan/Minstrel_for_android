package com.example.andrew.minstrel;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.andrew.minstrel.saves.SavesManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.andrew.minstrel.MainActivity.downloadImage;

/**
 * Created by andrew on 10.06.18.
 */

public class ProfileFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener{

    public static String ID;

    View rootView;
    AppBarLayout abl;
    SwipeRefreshLayout srl;
    ProgressBar pbAva, pbUser;
    Button butStories, butFriends, butFind;
    FloatingActionButton butInfo;
    TextView textStories, textFriends, textFollows, textPhotos, textVideos, textAudios;
    ImageView userPhoto;

    public User curUser;

    private ProfileInterface listener;

    public interface ProfileInterface {
        public void onDownloadComleted();
        public void onInfoClick(View v);
        public void onStoriesClick(View v);
    }

    public static ProfileFragment newInstance(String userID) {
        ProfileFragment fragment = new ProfileFragment();
        ID = userID;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_main, container, false);

        if (getActivity() instanceof ProfileInterface) {
            listener = (ProfileInterface) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString()
                    + " must implement MyListFragment.ProfileInterface");
        }

        abl = rootView.findViewById(R.id.appbar);

        srl = rootView.findViewById(R.id.swipe_profile);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                createUser(ID);
            }
        });
        srl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        butFind = rootView.findViewById(R.id.butFind);
        pbUser = rootView.findViewById(R.id.userProgress);

        butInfo = rootView.findViewById(R.id.butInfo);
        butFriends = rootView.findViewById(R.id.butFriends);
        textFriends = rootView.findViewById(R.id.textFriends);
        butStories = rootView.findViewById(R.id.butStories);
        textStories = rootView.findViewById(R.id.textStories);
        textFollows = rootView.findViewById(R.id.textFollows);
        textPhotos = rootView.findViewById(R.id.textPhotos);
        textVideos = rootView.findViewById(R.id.textVideos);
        textAudios = rootView.findViewById(R.id.textAudios);

        pbAva = rootView.findViewById(R.id.avaProgress);
        userPhoto = rootView.findViewById(R.id.userPhoto);

        //curUser = MainActivity.test(false, System.currentTimeMillis()-9000);
        //setCurUser();
        //listener.onDownloadComleted();
        createUser(ID);

        return rootView;
    }


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        srl.setEnabled(i == 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        abl.addOnOffsetChangedListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        abl.removeOnOffsetChangedListener(this);
    }


    public User getUser(){
        return curUser;
    }

    public void setCurUser(){
        try {
            final CheckBox cb = rootView.findViewById(R.id.checkBox);
            TextView firstName = rootView.findViewById(R.id.firstName);
            TextView secondName = rootView.findViewById(R.id.secondName);
            TextView online = rootView.findViewById(R.id.online);
            TextView screenName = rootView.findViewById(R.id.screenName);
            cb.setVisibility(View.VISIBLE);
            if (SavesManager.isSaved(curUser))
                cb.setChecked(true);
            else
                cb.setChecked(false);
            cb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (SavesManager.isSaved(curUser)){
                        SavesManager.deleteItem(curUser);
                        cb.setChecked(false);
                    }
                    else{
                        SavesManager.add(curUser);
                        cb.setChecked(true);
                    }
                }
            });

            firstName.setText(curUser.first_name);
            firstName.setVisibility(View.VISIBLE);
            secondName.setText(curUser.last_name);
            secondName.setVisibility(View.VISIBLE);
            if (curUser.online == 1)
                online.setText("Онлайн");
            else {
                Date d = new Date(curUser.last_seen * 1000);
                Date cur = new Date(System.currentTimeMillis());
                if (d.getDay() == cur.getDay() && d.getMonth() == cur.getMonth() && d.getYear() == cur.getYear())
                    online.setText("В сети: " + MainActivity.dfOnline1.format(d));
                else
                    online.setText("В сети: " + MainActivity.dfOnline2.format(d));
            }
            online.setVisibility(View.VISIBLE);
            String userLink = "<a href=\"https://vk.com/" + curUser.domain + "\">" + curUser.domain + "</a>";
            screenName.setText(Html.fromHtml(userLink, null, null));
            screenName.setMovementMethod(LinkMovementMethod.getInstance());
            screenName.setVisibility(View.VISIBLE);

                if (curUser.numFriends == 0) textFriends.setText("-");
            else textFriends.setText(Integer.toString(curUser.numFriends));
                if (curUser.numFollows == 0) textFollows.setText("-");
            else textFollows.setText(Integer.toString(curUser.numFollows));
                if (curUser.numPhotos == 0) textPhotos.setText("-");
            else textPhotos.setText(Integer.toString(curUser.numPhotos));
                if (curUser.numVideos == 0) textVideos.setText("-");
            else textVideos.setText(Integer.toString(curUser.numVideos));
                if (curUser.numAudios == 0) textAudios.setText("-");
            else textAudios.setText(Integer.toString(curUser.numAudios));

            //возможно ненужный код
            if (curUser.bigPhoto != null) {
                userPhoto.setImageBitmap(curUser.bigPhoto);
                userPhoto.setVisibility(View.VISIBLE);
            }

            if (curUser.arrStories == null)
                textStories.setText("-");
            else
                textStories.setText(Integer.toString(curUser.arrStories.length));
            //

        }catch (NullPointerException exc){
            System.err.println("NULL VALUE");
            exc.printStackTrace();
        }
    }

    public void createUser(String ID){
        UserCreator uc = new UserCreator();
        uc.execute(ID);
    }

    class UserCreator extends AsyncTask<String, Integer, User> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            userPhoto.setImageBitmap(null);
            //pbUser.setVisibility(View.VISIBLE);
            srl.setRefreshing(true);
        }

        @Override
        protected User doInBackground(String...param) {
            System.out.println("PROFILE");
            curUser = new User(param[0]);
            System.out.println(curUser);
            return curUser;
        }

        @Override
        protected void onPostExecute(User u) {
            super.onPostExecute(u);
            setCurUser();
            System.out.println("POINT THREAD");
            StoriesDownloader sd = new StoriesDownloader();
            sd.execute(curUser.ID);
            //FriendsDownloader fd = new FriendsDownloader();
            //fd.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, curUser.ID);
            AvaDownloader ad = new AvaDownloader();
            ad.execute(curUser.avaSmall, curUser.avaBig);
            //pbUser.setVisibility(View.INVISIBLE);
            srl.setRefreshing(false);
        }
    }

    class AvaDownloader extends AsyncTask<String, Integer, Bitmap> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            pbAva.setVisibility(View.VISIBLE);
        }

        @Override
        protected Bitmap doInBackground(String...param) {
            curUser.smallPhoto = downloadImage(param[0]);
            if (param.length > 1)
                curUser.bigPhoto = downloadImage(param[1]);
            return curUser.bigPhoto;
        }

        @Override
        protected void onPostExecute(Bitmap bm) {
            super.onPostExecute(bm);
            if (curUser.bigPhoto != null) {
                userPhoto.setImageBitmap(curUser.bigPhoto);
                userPhoto.setVisibility(View.VISIBLE);
            }
            pbAva.setVisibility(View.INVISIBLE);
            System.out.println("Ava is completed");
            listener.onDownloadComleted();
        }
    }

    /*class FriendsDownloader extends AsyncTask<Integer, Integer, Integer[]> {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            butFriends.setEnabled(false);
        }

        @Override
        protected Integer[] doInBackground(Integer...param) {
            Integer arr[] = getFriendsIds.getFriendsid(param[0]);
            return arr;
        }

        @Override
        protected void onPostExecute(Integer[] arr) {
            super.onPostExecute(arr);
            curUser.arrFriends = arr;
            if (curUser.arrFriends == null || curUser.arrFriends.length == 0){
                textFriends.setText("Нет");
            }
            else{
                textFriends.setText(Integer.toString(curUser.arrFriends.length));
                butFriends.setEnabled(true);
            }
            System.out.println("Friends are completed");
        }
    }*/

    class StoriesDownloader extends AsyncTask<Integer, Integer, Story[]>{
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            butStories.setEnabled(false);
        }

        @Override
        protected Story[] doInBackground(Integer...param) {
            System.out.println("STORY DOWNLOAD: "+param[0]);
            Stories stories = new Stories(param[0]);
            if (stories.length != 0){
                System.out.println("STORY: "+stories.storiesArray[0].url);
                curUser.numStories = stories.length;
                return stories.storiesArray;
            }
            else {
                curUser.numStories = 0;
                return null;
            }
        }

        @Override
        protected void onPostExecute(Story[] s) {
            super.onPostExecute(s);
            curUser.arrStories = s;
            if (curUser.arrStories == null) {
                textStories.setText("-");
                curUser.numStories = 0;
            }
            else{
                textStories.setText(Integer.toString(curUser.arrStories.length));
                curUser.numStories = curUser.arrStories.length;
                butStories.setEnabled(true);
            }
            System.out.println("Stories are completed");
        }
    }
}
