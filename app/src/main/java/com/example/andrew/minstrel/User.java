package com.example.andrew.minstrel;

import android.graphics.Bitmap;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class User extends LightUser implements Serializable{

    Story arrStories[];

    public String avaBig = "avaBig",
            status = "status",
            bdate = "bdate",
            country = "country",
            city = "city",
            occupType = "",
            occupName = "",
            site = "site",
            activities = "activities",
            about = "about";
    public int sex, blacklisted, numFriends, numFollows,
            numPhotos, numVideos, numAudios;
    public Bitmap bigPhoto;

    public User(){
        super();
    }

    public User(int id) {
        super(Integer.toString(id));
        try {
            ID = id;
            JsonObject object;
            String url = "https://api.vk.com/method/users.get?user_ids=" + id +
                    "&fields=photo_max_orig,status,bdate,city,occupation,country,site,sex,blacklisted,activities,about,counters"
                    + Constants.token + "&v=5.8";
            URL address = new URL(url);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            is.close();
            while (requestErrors.check(object)) {
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                is.close();
            }
            getInfo(object.getJsonArray("response").getJsonObject(0));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User(String screen_name) {
        super(screen_name);
        try {
            JsonObject object;
            String url = "https://api.vk.com/method/users.get?user_ids=" + screen_name +
                    "&fields=photo_max_orig,status,bdate,city,occupation,country,site,sex,blacklisted,activities,about,counters"
                    + Constants.token + "&v=5.8";
            URL address = new URL(url);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            is.close();
            while (requestErrors.check(object)) {
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                is.close();
            }
            getInfo(object.getJsonArray("response").getJsonObject(0));

        } catch (Exception e) {e.printStackTrace();}
    }

    void getInfo(JsonObject object){
        avaBig = object.getString("photo_max_orig");
        status = object.getString("status");
        if(object.containsKey(bdate))bdate = object.getString(bdate);
        if(object.containsKey(country))country = object.getJsonObject(country).getString("title");
        if(object.containsKey(city))city = object.getJsonObject(city).getString("title");
        if(object.containsKey("occupation")){
            occupType = object.getJsonObject("occupation").getString("type");
            if(object.getJsonObject("occupation").containsKey("name"))occupName = object.getJsonObject("occupation").getString("name");
        }
        if(object.containsKey(bdate))site = object.getString("site");
        sex = object.getInt("sex");
        blacklisted = object.getInt("blacklisted");
        numFriends = object.getJsonObject("counters").getInt("friends");
        numFollows = object.getJsonObject("counters").getInt("followers");
        numPhotos = object.getJsonObject("counters").getInt("photos");
        numVideos = object.getJsonObject("counters").getInt("videos");
        numAudios = object.getJsonObject("counters").getInt("audios");
        //System.out.println("point1");
        if(object.containsKey(activities))activities = object.getString(activities);
        //System.out.println("point2");
        if(object.containsKey(about))about = object.getString(about);
        System.out.println("ID: "+ID+" FRIENDS: "+numFriends+
                " FOLLOWS: "+numFollows+" PHOTOS: "+numPhotos+
                " VIDEOS: "+numVideos+" AUDIOS: "+numAudios);
    }
}



