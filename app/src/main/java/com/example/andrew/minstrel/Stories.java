package com.example.andrew.minstrel;

/**
 * Created by andrew on 15.04.18.
 */

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;

// Stories temp = new Stories(id/screen_name);
// Stories.storiesArray - array of Story
// Story has two fields : type(String) and url(String) - url of content
// Use length to avoid NullPointerException
// Stories now added to User

class Story implements Serializable{

    String type, url;
    long date;

    Story(){}

    Story(JsonObject object){
        type = object.getString("type");
        date = object.getInt("date");
        object = object.getJsonObject(type);
        if(type.equals("photo")){
            url = object.getString("photo_2560");
        }else{
            object = object.getJsonObject("files");
            url = object.getString("mp4_720");
        }
    }
}

public class Stories{
    public int length = 0;
    public Story[] storiesArray;

    public Stories(int id) {
        JsonObject object;
        String url = "https://api.vk.com/method/stories.get?owner_id=" + id + Constants.token + "&v=5.74";
        URL address = null;
        try {
            address = new URL(url);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            is.close();
            while (requestErrors.check(object)) {
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                is.close();
            }
            JsonArray arrayOfStories = object.getJsonObject("response").getJsonArray("items").getJsonArray(0);
            storiesArray = new Story[arrayOfStories.size()];
            for (int i = 0; i < storiesArray.length; i++) {
                storiesArray[i] = new Story(arrayOfStories.getJsonObject(i));
            }
            length = storiesArray.length;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
